package galeShapley;

import java.util.ArrayList;

public class Person {
	private String name;
	private int arrayPosition;
	private Person[] preferenceList;
	private boolean married;
	private int currentPreference;
	public Person() {
		married = false;
	}
	public Person(String n, Person[] preferences, int curPref, int arrayPos) {
		setName(n);
		setPreferenceList(preferences);
		setMarried(false);
		setCurrentPreference(curPref);
		setArrayPosition(arrayPos);
	}
	public boolean prefersXToY(Person x, Person y) {
		int i = preferenceList.length;
		for (int j = 0; j<preferenceList.length; j++) {
			if (preferenceList[j].getName().equalsIgnoreCase(x.getName()) && j < i) {
				i = j;
				return true;
			}
			else if (preferenceList[j].getName().equalsIgnoreCase(y.getName()) && j < i) {
				i = j;
				return false;
			}
		}
		return false;
	}
	public Person[] getPreferenceList() {
		return preferenceList;
	}
	public void setPreferenceList(Person[] preferenceList) {
		this.preferenceList = preferenceList;
	}
	
	public void setPreference(int pref, Person p) {
		preferenceList[pref] = p;
	}
	
	public boolean isMarried() {
		return married;
	}
	public void setMarried(boolean married) {
		this.married = married;
	}
	public int getCurrentPreference() {
		return currentPreference;
	}
	public void setCurrentPreference(int currentPreference) {
		this.currentPreference = currentPreference;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getArrayPosition() {
		return arrayPosition;
	}
	public void setArrayPosition(int arrayPosition) {
		this.arrayPosition = arrayPosition;
	}
}
