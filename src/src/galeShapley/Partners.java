package galeShapley;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Partners {
	public static void main (String[] args) {
		int partners = 3;
		Person[] Men = new Person[partners];
		Person[] Women = new Person[partners];
		Integer[][] Married = new Integer[3][2];	
		initialize(Men, Women, Married);
		
		Queue<Integer> Free = new LinkedList<Integer>();
		Free.add(0);
		Free.add(1);
		Free.add(2);
		while(!Free.isEmpty()) 
		{
			int i = Free.remove();
			Person m = Men[i];
			Person[] mPref = m.getPreferenceList();
			int highestRanked = m.getCurrentPreference();
			
			Person w = mPref[highestRanked];
			int wArrayPos = w.getArrayPosition();
			
			if (highestRanked<partners) {
				
				if (!w.isMarried()) {
					m.setMarried(true);
					w.setMarried(true);
					Married[i][0] = wArrayPos;
					Married[wArrayPos][1] = i;
					m.setCurrentPreference(highestRanked++);
				}
				else {
					if (!w.prefersXToY(m, Men[Married[wArrayPos][1]])) {
						Free.add(i);
						m.setCurrentPreference(highestRanked++);
					}
					else {
						Men[Married[wArrayPos][1]].setMarried(false);
						Free.add(Married[wArrayPos][1]);
						Married[i][0] = wArrayPos;
						Married[wArrayPos][1] = i;
						m.setCurrentPreference(highestRanked++);
						int wCurPref = w.getCurrentPreference();
						w.setCurrentPreference(wCurPref--);		
						m.setMarried(true);
					}
					
				}
			}
			
		}
		for (int i = 0; i < partners; i++) {
				System.out.println("Spouse: "  + Women[Married[i][0]].getName() + " Husband: " + Men[i].getName());
		}
	}
	
	public static void initialize(Person[] Men, Person[] Women, Integer[][] Married) {
		PopulateMen(Men);
		PopulateWomen(Women);
		LoadPreferences(Men, Women);
		for (int i = 0; i<3; i++) {
			Married[i][0] = -1;
			Married[i][1] = -1;
		}
	}
	
	public static void PopulateMen(Person[] Men) {
		Men[0] = new Person("Z", new Person[3], 0, 0);
		Men[1] = new Person("W", new Person[3], 0, 1);
		Men[2] = new Person("X", new Person[3], 0, 2);
	}
	public static void PopulateWomen(Person[] Women) {
		Women[0] = new Person("A", new Person[3], 2, 0);
		Women[1] = new Person("B", new Person[3], 2, 1);
		Women[2] = new Person("C", new Person[3], 2, 2);
	}
	public static void LoadPreferences(Person[] Men, Person[] Women) {
		Men[0].setPreference(0, Women[2]);
		Men[0].setPreference(1, Women[1]);
		Men[0].setPreference(2, Women[0]);
		
		Men[1].setPreference(0, Women[0]);
		Men[1].setPreference(1, Women[1]);
		Men[1].setPreference(2, Women[2]);
		
		Men[2].setPreference(0, Women[0]);
		Men[2].setPreference(1, Women[2]);
		Men[2].setPreference(2, Women[1]);
		
		Women[0].setPreference(0, Men[1]);
		Women[0].setPreference(1, Men[0]);
		Women[0].setPreference(2, Men[2]);
		
		Women[1].setPreference(0, Men[1]);
		Women[1].setPreference(1, Men[2]);
		Women[1].setPreference(2, Men[0]);
		
		Women[2].setPreference(0, Men[2]);
		Women[2].setPreference(1, Men[1]);
		Women[2].setPreference(2, Men[0]);
	}

}
